import "./styles.css";

import React from "react";
import { Parallax, ParallaxLayer } from "@react-spring/parallax";

import Sunny from "./images/sunny.png"

const Page = ({ offset, color }) => (
  <>
    <ParallaxLayer offset={offset} speed={0.2}>
      <div
        style={{
          width: "100%",
          height: "100%",
          backgroundImage: `url(${Sunny})`,
          backgroundSize: "contain"
        }}
      >
      </div>
    </ParallaxLayer>

    <ParallaxLayer offset={offset} speed={0.6}>
      <div className={`gradient ${color}`} />
    </ParallaxLayer>

    <ParallaxLayer className="text" offset={offset} speed={0.3}>
      <span>{offset + 1}</span>
    </ParallaxLayer>
  </>
);

export default function App() {
  return (
    <div>
      <Parallax className="container" pages={3} horizontal>
        <Page offset={0} color="orange" />
        <Page offset={1} color="teal" />
        <Page offset={2} color="pink" />
      </Parallax>
    </div>
  );
}
